import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './select-subgenre.styles.scss';
import { setCurrentStep, setSelectedSubgenre, setDescriptionRequire } from '../../store/actions/flow-state';
import { STEPS } from '../../config';
import StepsIndication from '../../components/steps-indication/steps-indication.component';

class SelectSubgenre extends React.Component {
  state = {
    step: STEPS.selectSubgenre,
    subgenres: []
  }

  componentDidMount() {
    const { setCurrentStepDispatch, genres, selectedGenre } = this.props;
    const { step } = this.state;
    setCurrentStepDispatch(step);
    const { subgenres } = genres.filter(genre => genre.id === selectedGenre)[0];
    this.setState({ subgenres })
  }

  onSubgenreSelect(id) {
    const { subgenres } = this.state;
    const { setSelectedSubgenreDispatch, setDescriptionRequireDispatch } = this.props;
    setSelectedSubgenreDispatch(id);
    const subgenre = subgenres.filter(subgenre => subgenre.id === id)[0];
    setDescriptionRequireDispatch(subgenre.isDescriptionRequired);
  }

  onSubmit() {
    const { history } = this.props;
    history.push({
      pathname: '/information'
    });
  }

  onBackAction() {
    const { history } = this.props;
    history.goBack();
  }

  onAddNewSubgenre() {
    const { history } = this.props;
    history.push({
      pathname: '/add-subgenre'
    });
  }

  render() {
    const { subgenres } = this.state;
    const { selectedSubgenre, currentStep } = this.props;
    return (
      <div className="container">
        <div className="row">
          <StepsIndication currentStep={currentStep} />
        </div>
        <div className="row items-box">
          {
            subgenres.map(subgenre => {
              return (
                <div className="col-4 col-sm-3 item-box" key={subgenre.id.toString()}>
                  <div className={`item card mb-3 ${selectedSubgenre === subgenre.id && 'active'}`}
                  >
                    <a onClick={() => this.onSubgenreSelect(subgenre.id)}>
                      <div className="card-body">
                        <h4 className="card-title">{subgenre.name}</h4>
                      </div>
                    </a>
                  </div>
                </div>
              )
            })
          }
          <div className="col-4 col-sm-3 item-box">
            <div className="item card mb-3">
              <a onClick={() => this.onAddNewSubgenre()}>
                <div className="card-body">
                  <h4 className="card-title">Add new</h4>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="footer-buttons col-12">
            <button type="button" onClick={() => this.onBackAction()} className="btn btn-secondary">Back</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" onClick={() => this.onSubmit()} disabled={!selectedSubgenre} className="btn btn-primary">Next</button>
          </div>
        </div>
      </div >
    )
  }
}

SelectSubgenre.propTypes = {
  selectedGenre: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  selectedSubgenre: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  genres: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired,
  setCurrentStepDispatch: PropTypes.func.isRequired,
  setSelectedSubgenreDispatch: PropTypes.func.isRequired,
  setDescriptionRequireDispatch: PropTypes.func.isRequired,
}

SelectSubgenre.defaultProps = {
  selectedSubgenre: null
}

const mapStateToProps = (state) => {
  return {
    genres: state.genresStore.genres,
    currentStep: state.flowStateStore.currentStep,
    selectedGenre: state.flowStateStore.selectedGenre,
    selectedSubgenre: state.flowStateStore.selectedSubgenre
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentStepDispatch: value => dispatch(setCurrentStep(value)),
    setSelectedSubgenreDispatch: value => dispatch(setSelectedSubgenre(value)),
    setDescriptionRequireDispatch: value => dispatch(setDescriptionRequire(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectSubgenre);