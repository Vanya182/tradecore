import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './select-genre.styles.scss';
import { setCurrentStep, setSelectedGenre } from '../../store/actions/flow-state';
import { STEPS } from '../../config';
import StepsIndication from '../../components/steps-indication/steps-indication.component';

class SelectGenre extends React.Component {
  state = {
    step: STEPS.selectGenre
  }

  componentDidMount() {
    const { setCurrentStepDispatch } = this.props;
    const { step } = this.state;
    setCurrentStepDispatch(step);
  }

  onGenreSelect(id) {
    const { setSelectedGenreDispatch } = this.props;
    setSelectedGenreDispatch(id);
  }

  onSubmit() {
    const { history } = this.props;
    history.push({
      pathname: '/subgenre'
    });
  }

  render() {
    const { genres, selectedGenre, currentStep } = this.props;
    return (
      <div className="container">
        <div className="row">
          <StepsIndication currentStep={currentStep} />
        </div>
        <div className="row items-box">
          {
            genres.map(genre => {
              return (
                <div className="col-4 col-sm-3 item-box" key={genre.id.toString()}>
                  <div className={`item card mb-3 ${selectedGenre === genre.id && 'active'}`}
                  >
                    <a onClick={() => this.onGenreSelect(genre.id)}>
                      <div className="card-body">
                        <h4 className="card-title">{genre.name}</h4>
                      </div>
                    </a>
                  </div>
                </div>
              )
            })
          }
        </div>
        <div className="row">
          <div className="footer-buttons col-12 text-align-right">
            <button type="button" onClick={() => this.onSubmit()} disabled={!selectedGenre} className="btn btn-primary">Next</button>
          </div>
        </div>
      </div>
    )
  }
}

SelectGenre.propTypes = {
  currentStep: PropTypes.string.isRequired,
  selectedGenre: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  genres: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired,
  setCurrentStepDispatch: PropTypes.func.isRequired,
  setSelectedGenreDispatch: PropTypes.func.isRequired,
}

SelectGenre.defaultProps = {
  selectedGenre: null
}

const mapStateToProps = (state) => {
  return {
    genres: state.genresStore.genres,
    currentStep: state.flowStateStore.currentStep,
    selectedGenre: state.flowStateStore.selectedGenre
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentStepDispatch: value => dispatch(setCurrentStep(value)),
    setSelectedGenreDispatch: value => dispatch(setSelectedGenre(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectGenre);