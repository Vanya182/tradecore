import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setCurrentStep, setSelectedSubgenre, setSubgenreToAddInformationKey, setDescriptionRequire, resetSubgenreToAddInformation } from '../../store/actions/flow-state';
import { addSubgenre } from '../../store/actions/genres';
import { STEPS } from '../../config';
import StepsIndication from '../../components/steps-indication/steps-indication.component';
import SubgenreInformationForm from '../../components/subgenre-information-form/subgenre-information-form.component';

const shortid = require('shortid');

class AddSubgenre extends React.Component {
  state = {
    id: null,
    loading: true,
    step: STEPS.addSubgenre
  }

  componentDidMount() {
    const { setCurrentStepDispatch, setSubgenreToAddInformationKeyDispatch } = this.props;
    const { step } = this.state;
    setCurrentStepDispatch(step);
    const id = shortid.generate();
    this.setState({ id, loading: false });
    setSubgenreToAddInformationKeyDispatch({ key: 'id', value: id })
  }

  onBackAction() {
    const { history } = this.props;
    history.goBack();
  }

  onSubmit() {
    this.refs.subgenreInformationForm.doSubmit();
  }

  handleSubmit() {
    const { history, addSubgenreDispatch, selectedGenre, subgenreToAddInformation, setDescriptionRequireDispatch, setSelectedSubgenreDispatch, resetSubgenreToAddInformationDispatch } = this.props;

    addSubgenreDispatch({
      genre: selectedGenre,
      subgenreToAddInformation
    });
    setDescriptionRequireDispatch(subgenreToAddInformation.isDescriptionRequired);
    setSelectedSubgenreDispatch(subgenreToAddInformation.id);
    resetSubgenreToAddInformationDispatch();
    history.push({
      pathname: '/information'
    });
  }

  render() {
    const { loading, id } = this.state;
    const { currentStep, subgenreToAddInformation, setSubgenreToAddInformationKeyDispatch } = this.props;
    return (
      !loading && (<div className="container">
        <div className="row">
          <StepsIndication currentStep={currentStep} />
        </div>
        <div className="row items-box">
          <SubgenreInformationForm ref='subgenreInformationForm' handleSubmit={() => { this.handleSubmit() }} subgenreToAddInformation={{ id, ...subgenreToAddInformation }} setSubgenreToAddInformationKeyDispatch={setSubgenreToAddInformationKeyDispatch} />
        </div>
        <div className="row">
          <div className="footer-buttons col-12">
            <button type="button" onClick={() => this.onBackAction()} className="btn btn-secondary">Back</button>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" onClick={() => this.onSubmit()} className="btn btn-primary">Next</button>
          </div>
        </div>
      </div >)
    )
  }
}

AddSubgenre.propTypes = {
  currentStep: PropTypes.string.isRequired,
  selectedGenre: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  subgenreToAddInformation: PropTypes.oneOfType([PropTypes.object]).isRequired,
  addSubgenreDispatch: PropTypes.func.isRequired,
  setCurrentStepDispatch: PropTypes.func.isRequired,
  setDescriptionRequireDispatch: PropTypes.func.isRequired,
  setSelectedSubgenreDispatch: PropTypes.func.isRequired,
  resetSubgenreToAddInformationDispatch: PropTypes.func.isRequired,
  setSubgenreToAddInformationKeyDispatch: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => {
  return {
    currentStep: state.flowStateStore.currentStep,
    selectedGenre: state.flowStateStore.selectedGenre,
    subgenreToAddInformation: state.flowStateStore.subgenreToAddInformation
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentStepDispatch: value => dispatch(setCurrentStep(value)),
    setSubgenreToAddInformationKeyDispatch: value => dispatch(setSubgenreToAddInformationKey(value)),
    setSelectedSubgenreDispatch: value => dispatch(setSelectedSubgenre(value)),
    setDescriptionRequireDispatch: value => dispatch(setDescriptionRequire(value)),
    resetSubgenreToAddInformationDispatch: value => dispatch(resetSubgenreToAddInformation(value)),
    addSubgenreDispatch: value => dispatch(addSubgenre(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddSubgenre);