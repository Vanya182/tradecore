import React from 'react';

class BookAdded extends React.Component {
  state = {

  }

  onAddNewBook() {
    const { history } = this.props;
    history.push({
      pathname: '/'
    });
  }

  render() {
    return (
      <div className="jumbotron">
        <h1 className="text-align-center">Book added successfully</h1>
        <p className="lead text-align-center">
          <br />
          <br />
          <a className="btn color-white btn-primary btn-lg" onClick={() => { this.onAddNewBook() }} role="button">Add another book</a>
        </p>
      </div>
    )
  }
}

export default BookAdded;