
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './book-infortmation.styles.scss';
import { setCurrentStep, resetFlowState, setBookInformationKey } from '../../store/actions/flow-state';
import { addBook } from '../../store/actions/books';
import { STEPS } from '../../config';
import StepsIndication from '../../components/steps-indication/steps-indication.component';
import BookInformationForm from '../../components/book-information-form/book-information-form.component';

class BookInformation extends React.Component {
    state = {
        step: STEPS.fillBookInfo,
    }

    componentDidMount() {
        const { setCurrentStepDispatch } = this.props;
        const { step } = this.state;
        setCurrentStepDispatch(step);
    }

    onSubmit() {
        this.refs.bookInformationForm.doSubmit();
    }

    onFormValid() {
        const { history, addBookDispatch, resetFlowStateDispatch, bookInformation, selectedGenre, selectedSubgenre } = this.props;
        history.push({
            pathname: '/book-added'
        });
        const payload = {
            genre: selectedGenre,
            subgenre: selectedSubgenre,
            ...bookInformation
        };
        console.log('ADD BOOK');
        console.log(payload);
        addBookDispatch(payload);
        resetFlowStateDispatch();
    }

    onBackAction() {
        const { history } = this.props;
        history.goBack();
    }

    render() {
        const { currentStep, bookInformation, setBookInformationKeyDispatch, isDescriptionRequired } = this.props;
        return (
            <div className="container">
                <div className="row">
                    <StepsIndication currentStep={currentStep} />
                </div>
                <div className="row">
                    <BookInformationForm handleSubmit={() => { this.onFormValid() }} ref='bookInformationForm' isDescriptionRequired={isDescriptionRequired} bookInformation={bookInformation} setBookInformationKeyDispatch={setBookInformationKeyDispatch} />
                </div>
                <div className="row">
                    <div className="footer-buttons col-12 text-align-right">
                        <button type="button" onClick={() => this.onBackAction()} className="btn btn-secondary">Back</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" onClick={() => this.onSubmit()} className="btn btn-primary">Complete</button>
                    </div>
                </div>
            </div>
        )
    }
}

BookInformation.propTypes = {
    currentStep: PropTypes.string.isRequired,
    isDescriptionRequired: PropTypes.bool,
    selectedGenre: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    selectedSubgenre: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    bookInformation: PropTypes.oneOfType([PropTypes.object]).isRequired,
    books: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired,
    setCurrentStepDispatch: PropTypes.func.isRequired,
    addBookDispatch: PropTypes.func.isRequired,
    setBookInformationKeyDispatch: PropTypes.func.isRequired,
    resetFlowStateDispatch: PropTypes.func.isRequired,
}

BookInformation.defaultProps = {
    isDescriptionRequired: false
}

const mapStateToProps = (state) => {
    return {
        currentStep: state.flowStateStore.currentStep,
        bookInformation: state.flowStateStore.bookInformation,
        books: state.booksStore.books,
        isDescriptionRequired: state.flowStateStore.isDescriptionRequired,
        selectedGenre: state.flowStateStore.selectedGenre,
        selectedSubgenre: state.flowStateStore.selectedSubgenre
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setCurrentStepDispatch: value => dispatch(setCurrentStep(value)),
        setBookInformationKeyDispatch: value => dispatch(setBookInformationKey(value)),
        addBookDispatch: value => dispatch(addBook(value)),
        resetFlowStateDispatch: () => dispatch(resetFlowState())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BookInformation);