export const SET_CURRENT_STEP = 'SET_CURRENT_STEP';
export const SET_SELECTED_GENRE = 'SET_SELECTED_GENRE';
export const SET_SELECTED_SUBGENRE = 'SET_SELECTED_SUBGENRE';
export const SET_BOOK_INFORMATION_KEY = 'SET_BOOK_INFORMATION_KEY';
export const RESET_FLOW_STATE = 'RESET_FLOW_STATE';
export const RESET_SUBGENRE_TO_ADD_INFORMATION = 'RESET_SUBGENRE_TO_ADD_INFORMATION';
export const SET_DESCRIPTION_REQUIRE = 'SET_DESCRIPTION_REQUIRE';
export const SET_SUBGENRE_TO_ADD_INFORMATION_KEY = 'SET_SUBGENRE_TO_ADD_INFORMATION_KEY';
export const ADD_SUBGENRE = 'ADD_SUBGENRE';
export const ADD_BOOK = 'ADD_BOOK';