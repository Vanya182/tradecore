import { createStore, applyMiddleware } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';

const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel2
};

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default function configureStore(initialState = {}) {
  let middleware = [];
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    // const logger = createLogger({ collapsed: true });
    // middleware = [...middleware, logger];
    middleware = [...middleware];
    middleware = [...middleware];
  } else {
    middleware = [...middleware];
  }

  const store = createStore(persistedReducer, initialState,
    applyMiddleware(...middleware));
  const persistor = persistStore(store);

  // UNCOMMENT TO CLEAR THE PERSISTED STORE
  // persistor.purge();
  return { store, persistor };
}