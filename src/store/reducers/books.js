import { ADD_BOOK } from '../types';

const INITIAL_STATE = {
  books: []
};

export default function booksReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ADD_BOOK:
      const books = [...state.books, action.payload];
      console.log(`BOOKS IN STORE: ${books.length}`);
      console.table(books);
      return {
        ...state,
        books
      }
    default:
      break;
  }
  return state;
}