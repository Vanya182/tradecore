import { SET_CURRENT_STEP, SET_SELECTED_GENRE, SET_SELECTED_SUBGENRE, SET_BOOK_INFORMATION_KEY, SET_DESCRIPTION_REQUIRE, SET_SUBGENRE_TO_ADD_INFORMATION_KEY, RESET_SUBGENRE_TO_ADD_INFORMATION, RESET_FLOW_STATE } from '../types';
import { STEPS } from '../../config';

const INITIAL_STATE = {
  currentStep: STEPS.selectGenre,
  selectedGenre: null,
  selectedSubgenre: null,
  bookInformation: {},
  subgenreToAddInformation: {},
  isDescriptionRequired: false
};

export default function flowStateReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_CURRENT_STEP: {
      return {
        ...state,
        currentStep: action.payload
      }
    }
    case SET_SELECTED_GENRE: {
      return {
        ...state,
        selectedGenre: action.payload,
        selectedSubgenre: null,
        subgenreToAddInformation: {}
      }
    }
    case SET_SELECTED_SUBGENRE: {
      return {
        ...state,
        selectedSubgenre: action.payload
      }
    }
    case SET_BOOK_INFORMATION_KEY: {
      const payload = {};
      payload[action.payload.key] = action.payload.value;
      return {
        ...state,
        bookInformation: { ...state.bookInformation, ...payload }
      }
    }
    case SET_SUBGENRE_TO_ADD_INFORMATION_KEY: {
      const payload = {};
      payload[action.payload.key] = action.payload.value;
      return {
        ...state,
        subgenreToAddInformation: { ...state.subgenreToAddInformation, ...payload }
      }
    }
    case RESET_SUBGENRE_TO_ADD_INFORMATION: {
      return {
        ...state,
        subgenreToAddInformation: {}
      }
    }
    case RESET_FLOW_STATE: {
      return {
        ...INITIAL_STATE
      }
    }
    case SET_DESCRIPTION_REQUIRE: {
      return {
        ...state,
        isDescriptionRequired: action.payload
      }
    }
    default:
      return state;
  }
}