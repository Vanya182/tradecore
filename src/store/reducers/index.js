import { combineReducers } from 'redux';
import genresReducer from './genres';
import flowStateReducer from './flow-state';
import booksReducer from './books';

const rootReducer = combineReducers({
    genresStore: genresReducer,
    flowStateStore: flowStateReducer,
    booksStore: booksReducer
});

export default rootReducer;
