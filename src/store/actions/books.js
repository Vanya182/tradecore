import { ADD_BOOK } from '../types';

export function addBook(value) {
    return {
        type: ADD_BOOK,
        payload: value
    };
}