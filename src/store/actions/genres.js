import { ADD_SUBGENRE } from '../types';

export function addSubgenre(value) {
    return {
        type: ADD_SUBGENRE,
        payload: value
    };
}