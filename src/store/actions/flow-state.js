import { SET_SELECTED_GENRE, SET_SELECTED_SUBGENRE, SET_CURRENT_STEP, SET_BOOK_INFORMATION_KEY, SET_DESCRIPTION_REQUIRE, SET_SUBGENRE_TO_ADD_INFORMATION_KEY, RESET_FLOW_STATE, RESET_SUBGENRE_TO_ADD_INFORMATION } from '../types';

export function setCurrentStep(value) {
    return {
        type: SET_CURRENT_STEP,
        payload: value
    };
}

export function setSelectedGenre(value) {
    return {
        type: SET_SELECTED_GENRE,
        payload: value
    };
}

export function setSelectedSubgenre(value) {
    return {
        type: SET_SELECTED_SUBGENRE,
        payload: value
    };
}

export function setBookInformationKey(value) {
    return {
        type: SET_BOOK_INFORMATION_KEY,
        payload: value
    };
}

export function setSubgenreToAddInformationKey(value) {
    return {
        type: SET_SUBGENRE_TO_ADD_INFORMATION_KEY,
        payload: value
    };
}

export function setDescriptionRequire(value) {
    return {
        type: SET_DESCRIPTION_REQUIRE,
        payload: value
    };
}

export function resetSubgenreToAddInformation() {
    return {
        type: RESET_SUBGENRE_TO_ADD_INFORMATION
    };
}

export function resetFlowState() {
    return {
        type: RESET_FLOW_STATE
    };
}
