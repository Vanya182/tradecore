const STEPS = {
    'selectGenre': 'SELECT_GENRE',
    'selectSubgenre': 'SELECT_SUBGENRE',
    'addSubgenre': 'ADD_SUBGENRE',
    'fillBookInfo': 'FILL_BOOK_INFO'
};

export { STEPS }