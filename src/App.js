import React from 'react';
import './App.scss';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './store';
import SelectGenre from './containers/select-genre/select-genre.container';
import SelectSubgenre from './containers/select-subgenre/select-subgenre.container';
import AddSubgenre from './containers/add-subgenre/add-subgenre.container';
import BookInformation from './containers/book-infortmation/book-infortmation.container';
import BookAdded from './containers/book-added/book-added.container';

const { store, persistor } = configureStore();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <div className="app-container container">
            <div className="app-page">
              <Route exact path='/' component={SelectGenre} />
              <Route exact path='/subgenre' component={SelectSubgenre} />
              <Route exact path='/add-subgenre' component={AddSubgenre} />
              <Route exact path='/information' component={BookInformation} />
              <Route exact path='/book-added' component={BookAdded} />
            </div>
          </div>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}

export default App;
