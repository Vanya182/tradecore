import React from 'react';
import PropTypes from 'prop-types';
import './subgenre-information.styles.scss';

class SubgenreInformationForm extends React.Component {
  state = {
    form: null,
    formFieldsWithErrors: [],
    formFields: [
      {
        key: 'name',
        placeholder: 'Subgenre name',
        input: 'input',
        type: 'text',
        required: true
      },
      {
        key: 'description',
        placeholder: 'Subgenre description',
        input: 'textarea',
        type: 'text',
        required: false
      },
      {
        key: 'isDescriptionRequired',
        placeholder: 'Description is required for this subgenre',
        input: 'checkbox',
        type: 'checkbox',
        required: false
      }
    ]
  }

  componentDidMount() {
    const { formFields } = this.state;
    const { subgenreToAddInformation } = this.props;
    const stateFields = {};
    formFields.map(field => { stateFields[field.key] = subgenreToAddInformation[field.key] || ''; return field; });
    const state = { ...this.state, ...stateFields };
    this.setState({ ...state });
  }

  handleChange(event, key, isCheckbox = false) {
    const { setSubgenreToAddInformationKeyDispatch } = this.props;
    setSubgenreToAddInformationKeyDispatch({
      key,
      value: isCheckbox ? event.target.checked : event.target.value
    });
  }

  doSubmit() {
    const { subgenreToAddInformation, handleSubmit } = this.props;
    const { formFields } = this.state;
    const formFieldsWithErrors = [];
    formFields.filter(field => field.required)
      .map(field => {
        const fieldValue = subgenreToAddInformation[field.key];
        if (!fieldValue) {
          formFieldsWithErrors.push(field.key);
        }
        return field;
      });
    if (formFieldsWithErrors.length) {
      this.setState({ formFieldsWithErrors });
      return false;
    }
    handleSubmit();
    return true;
  }

  render() {
    const { formFields, formFieldsWithErrors } = this.state;
    const { subgenreToAddInformation } = this.props;
    return (
      <form ref='form' className="full-width">
        {formFields.map(field => {
          const fieldValue = subgenreToAddInformation[field.key];
          return (
            <div className={`form-group col-12 mb-3 ${(formFieldsWithErrors.indexOf(field.key) >= 0) && 'has-danger'}`} key={field.key}>
              <label className="col-form-label" htmlFor={field.key}>{field.placeholder}</label>
              {
                (field.input === 'input') && (<input type={field.type} className="form-control" value={fieldValue || ''} onChange={event => this.handleChange(event, field.key)} placeholder={field.placeholder} id={field.key} />)
              }
              {
                (field.input === 'textarea') && (<textarea type={field.type} className="form-control" value={fieldValue || ''} onChange={event => this.handleChange(event, field.key)} placeholder={field.placeholder} id={field.key} />)
              }
              {
                (field.input === 'checkbox') && (<input type={field.type} checked={!!fieldValue} className="form-control" onChange={event => this.handleChange(event, field.key, true)} placeholder={field.placeholder} id={field.key} />)
              }
            </div >
          )
        })}
      </form>
    )
  }
}

SubgenreInformationForm.propTypes = {
  subgenreToAddInformation: PropTypes.oneOfType([PropTypes.object]).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  setSubgenreToAddInformationKeyDispatch: PropTypes.func.isRequired,
}

export default SubgenreInformationForm;