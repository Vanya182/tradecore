import React from 'react';
import './steps-indication.styles.scss';
import PropTypes from 'prop-types';
import { STEPS } from '../../config';

const StepsIndication = props => {
    const { currentStep } = props;

    return (
        <div className="step-box">
            <div className="row">
                <div className={`col-3 step ${currentStep === STEPS.selectGenre && 'active'}`}>
                    <div className="step-circle">1</div>
                    <div className="step-name">Genre</div>
                </div>
                <div className={`col-3 step ${currentStep === STEPS.selectSubgenre && 'active'}`}>
                    <div className="step-circle">2</div>
                    <div className="step-name">Subgenre</div>
                </div>
                <div className={`col-3 step ${currentStep === STEPS.addSubgenre && 'active'}`}>
                    <div className="step-circle">3</div>
                    <div className="step-name">Add Subgenre</div>
                </div>
                <div className={`col-3 step ${currentStep === STEPS.fillBookInfo && 'active'}`}>
                    <div className="step-circle">4</div>
                    <div className="step-name">Information</div>
                </div>
            </div>
        </div>
    );
}

StepsIndication.propTypes = {
    currentStep: PropTypes.string
}

StepsIndication.defaultProps = {
    currentStep: STEPS.selectGenre
}

export default StepsIndication;