import React from 'react';
import './book-information-form.styles.scss';
import PropTypes from 'prop-types';

class BookInformationForm extends React.Component {
  state = {
    form: null,
    formFieldsWithErrors: [],
    formFields: [
      {
        key: 'title',
        placeholder: 'Title',
        input: 'input',
        type: 'text',
        required: true
      },
      {
        key: 'author',
        placeholder: 'Author',
        input: 'input',
        type: 'text',
        required: true
      },
      {
        key: 'isbn',
        placeholder: 'ISBN',
        input: 'input',
        type: 'text'
      },
      {
        key: 'publisher',
        placeholder: 'Publisher',
        input: 'input',
        type: 'text'
      },
      {
        key: 'date',
        placeholder: 'Date published',
        input: 'input',
        type: 'date'
      },
      {
        key: 'numberOfPage',
        placeholder: 'Number of pages',
        input: 'input',
        type: 'number'
      },
      {
        key: 'format',
        placeholder: 'Format',
        input: 'input',
        type: 'text'
      },
      {
        key: 'edition',
        placeholder: 'Edition',
        input: 'input',
        type: 'text'
      },
      {
        key: 'editionLanguage',
        placeholder: 'Edition language',
        input: 'input',
        type: 'text'
      },
      {
        key: 'description',
        placeholder: 'Description',
        input: 'textarea',
        type: 'textarea',
        required: this.props.isDescriptionRequired
      }
    ]
  }

  componentDidMount() {
    const { formFields } = this.state;
    const { bookInformation } = this.props;
    const stateFields = {};
    formFields.map(field => { stateFields[field.key] = bookInformation[field.key] || ''; return field });
    const state = { ...this.state, ...stateFields };
    this.setState({ ...state });
  }

  handleChange(event, key) {
    const { setBookInformationKeyDispatch } = this.props;
    // const payload = {};
    // payload[key] = event.target.value;
    // this.setState({ ...payload })
    setBookInformationKeyDispatch({
      key,
      value: event.target.value
    });
  }

  doSubmit() {
    const { bookInformation, handleSubmit } = this.props;
    const { formFields } = this.state;
    const formFieldsWithErrors = [];
    formFields.filter(field => field.required)
      .map(field => {
        const fieldValue = bookInformation[field.key];
        if (!fieldValue) {
          formFieldsWithErrors.push(field.key);
        }
      });
    if (formFieldsWithErrors.length) {
      this.setState({ formFieldsWithErrors });
      return false;
    }
    handleSubmit();
    return true;
  }

  render() {
    const { formFields, formFieldsWithErrors } = this.state;
    const { bookInformation } = this.props;
    return (
      <form ref="form" className="full-width">
        {formFields.map(field => {
          const fieldValue = bookInformation[field.key];
          return (
            <div className={`form-group col-12 mb-3 ${(formFieldsWithErrors.indexOf(field.key) >= 0) && 'has-danger'}`} key={field.key}>
              <label className="col-form-label" htmlFor={field.key}>{field.placeholder}</label>
              {
                field.input === 'input' ? (
                  <input type={field.type} className="form-control" value={fieldValue || ''} onChange={event => this.handleChange(event, field.key)} placeholder={field.placeholder} id={field.key} />
                ) : (
                    <textarea type={field.type} className="form-control" value={fieldValue || ''} onChange={event => this.handleChange(event, field.key)} placeholder={field.placeholder} id={field.key} />
                  )}
            </div >
          )
        })}
      </form>
    )
  }
}

BookInformationForm.propTypes = {
  bookInformation: PropTypes.oneOfType([PropTypes.object]).isRequired,
  isDescriptionRequired: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  setBookInformationKeyDispatch: PropTypes.func.isRequired
}

BookInformationForm.defaultProps = {
  isDescriptionRequired: false
}

export default BookInformationForm;